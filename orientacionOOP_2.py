class Coordenada:
    def __init__(self, lat, lon):
        self.latitud = lat
        self.longitud = lon
    
    def Escribe(self):
        lon = self.longitud
        lat = self.latitud
        print('La latitud es ',lat, 'y la longitud es ',lon)
    
    
class Ciudad:
    def __init__(self, n, p, c):
        self.nombre = n
        self.poblacion = p
        self.coordenada = c
    
    def Escribe(self):
        n = self.nombre 
        p = self.poblacion
        c = self.coordenada
        print(n,'tiene ',p,'habitantes y sus coordenadas son:',c)   


CoordenadaMadrid = Coordenada(40.41831, -3.70275)
CoordenadaSevilla = Coordenada(37.38283, -5.97317)

CiudadMadrid = Ciudad('Madrid', 3233000, CoordenadaMadrid)
CiudadSevilla = Ciudad('Sevilla', 693229, CoordenadaSevilla)



CiudadMadrid.Escribe()
CiudadSevilla.Escribe()

