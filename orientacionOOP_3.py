class Coordenada:
    def __init__(self, lat, lon):
        self.latitud = lat
        self.longitud = lon
    
    def __str__(self):
        return ('{}, {}'.format(self.longitud, self.latitud))
        
class Ciudad:
    def __init__(self, n, p, c):
        self.nombre = n
        self.poblacion = p
        self.coordenada = c
    
    def __str__(self):
        return ('La ciudad de {} tiene coordenadas {} y tiene {} habitantes'.format
                (self.nombre, self.coordenada, self.poblacion))




CoordenadaMadrid = Coordenada(40.41831, -3.70275)
CoordenadaSevilla = Coordenada(37.38283, -5.97317)

CiudadMadrid = Ciudad('Madrid', 3233000, CoordenadaMadrid)
CiudadSevilla = Ciudad('Sevilla', 693229, CoordenadaSevilla)


print(CiudadMadrid)
print(CiudadSevilla)